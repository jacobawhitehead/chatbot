//
//  SessionManager.swift
//  ChatBot
//
//  Created by Jacob Whitehead on 06/06/2016.
//  Copyright © 2016 Schibsted. All rights reserved.
//

import Foundation

class SessionManager {
    private var username = ""
    var firstName = ""
    var lastName = ""
    
    private let preferences = NSUserDefaults.standardUserDefaults()
    private let usernamePreferencesKey = "username"
    
    static let currentSession = SessionManager()
    private init(){}
    
    func loginWithUsername(username: String) {
        self.username = username
        self.storeUsername()
        self.splitFullName()
    }
    
    func isLoggedIn() -> Bool {
        username = getStoredUsername()
        
        return username != ""
    }
    
    func currentUser() -> String {
        return self.username
    }
    
    func logout() {
        self.username = ""
        preferences.removeObjectForKey(usernamePreferencesKey)
    }
    
    private func storeUsername() {
        preferences.setValue(username, forKey: usernamePreferencesKey)
        preferences.synchronize()
    }
    
    private func getStoredUsername() -> String {
        return preferences.objectForKey(usernamePreferencesKey) as? String ?? ""
    }
    
    private func splitFullName() {
        let fullName = self.username
        let fullNameArr = fullName.characters.split{$0 == " "}.map(String.init)
        
        self.firstName = fullNameArr[0]
        self.lastName = fullNameArr[1]
        
    }
}