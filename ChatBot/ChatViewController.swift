//
//  ChatViewController.swift
//  ChatBot
//
//  Created by Muge Ersoy on 21/04/2016.
//  Copyright © 2016 Schibsted. All rights reserved.
//

import UIKit

let kChatIdentifier = "ChatBubble"
let kUserChatIdentifier = "UserChatBubble"
let kEventMessagesRetrieved = "MessageRetrieved"

class ChatViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, MessageServiceDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var toolbarBottomConstraint: NSLayoutConstraint!
    private var initialToolbarConstraint:CGFloat = 0.0
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var toolbar: UIView!
    
    var messages = [Message]()

    let messageServiceController = MessageServiceController()
    
    override func viewDidLoad() {
        self.tableView.registerNib(UINib.init(nibName: "ChatBubble", bundle: nil), forCellReuseIdentifier: kChatIdentifier)
        self.tableView.registerNib(UINib.init(nibName: "UserChatBubble", bundle: nil), forCellReuseIdentifier: kUserChatIdentifier)
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        self.textfield.delegate = self
        
        self.initialToolbarConstraint = toolbarBottomConstraint.constant
        
        let title = "Chat - " + SessionManager.currentSession.currentUser()
        self.titleLabel.text = title
        
        messageServiceController.delegate = self
        messageServiceController.retrieveMessages()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200;
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }

    @IBAction func logoutButtonTapped(sender: AnyObject) {
        self.presentLogoutActionSheet()
    }
    
    @IBAction func sendButtonTapped(sender: AnyObject) {
        if self.textfield.text != "" {
            self.postChat()
        } else {
            self.presentAlertWithTitle("Enter something first!", message: "You need to write something to send a message!")
        }
    }
    
    private func presentAlertWithTitle(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    private func postChat() {
        let message = Message(content: self.textfield.text!)
        self.messages.append(message)
        
        let row = self.messages.count - 1
        let indexPath = NSIndexPath(forRow: row, inSection: 0)
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        
        self.view.endEditing(true)
        self.textfield.text = ""
        scrollToBottom()
    }
    
    private func scrollToBottom() {
        if self.tableView.contentSize.height > self.tableView.frame.size.height {
            self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: messages.count - 1, inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
        }
    }
    
    //MARK - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (messages.count > 0) {
            return self.messages.count
        } else {
            self.tableView.backgroundView = UIView(frame: self.tableView.frame)
            return 0
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let message = self.messages[indexPath.row]
        var cell: UITableViewCell!
        
        if message.username == SessionManager.currentSession.currentUser() {
            let userChatCell = tableView.dequeueReusableCellWithIdentifier(kUserChatIdentifier) as! UserChatBubble
            userChatCell.updateChatBubble(message)
            cell = userChatCell
        } else {
            let standardCell = tableView.dequeueReusableCellWithIdentifier(kChatIdentifier) as! ChatBubble
            standardCell.updateChatBubble(message)
            cell = standardCell
        }
        
        return cell
    }
    
    //MARK - MessageServiceDelegate
    func messagesLoaded() {
        self.messages = messageServiceController.getMessages()
        
        dispatch_async(dispatch_get_main_queue(), {
            let indexSet = NSIndexSet(index: 0)
            self.tableView.reloadSections(indexSet, withRowAnimation: .Fade)
        })
    }
    
    //MARK - Action Sheet
    private func presentLogoutActionSheet() {
        let optionMenu = UIAlertController(title: nil, message: "Logout", preferredStyle: .ActionSheet)
        
        let logoutAction = UIAlertAction(title: "Logout", style: .Destructive, handler: {
            (alert: UIAlertAction!) -> Void in
            
            SessionManager.currentSession.logout()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            UIApplication.sharedApplication().keyWindow?.rootViewController = storyboard.instantiateViewControllerWithIdentifier("loginViewController")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(logoutAction)
        optionMenu.addAction(cancelAction)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    //MARK - Keyboard
    func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        UIView.animateWithDuration(duration) { () -> Void in
            self.toolbarBottomConstraint.constant = keyboardFrame.size.height
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        UIView.animateWithDuration(duration) { () -> Void in
            self.toolbarBottomConstraint.constant = self.initialToolbarConstraint
            self.view.layoutIfNeeded()
        }
    }
}