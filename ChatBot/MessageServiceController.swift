//
//  MessageServiceController.swift
//  ChatBot
//
//  Created by Jacob Whitehead on 06/06/2016.
//  Copyright © 2016 Schibsted. All rights reserved.
//

import Foundation

protocol MessageServiceDelegate {
    func messagesLoaded()
}

class MessageServiceController {
    private let serverURL =  "https://s3-eu-west-1.amazonaws.com/rocket-interview/chat.json"
    private var messages = [Message]()
    
    var delegate: MessageServiceDelegate?
    
    func retrieveMessages(){
        let url = NSURL(string: serverURL)
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
            if data != nil && error == nil {
                self.parseJSON(JSON(data: data!))
                self.delegate?.messagesLoaded()
            }
        }
        
        task.resume()
    }
    
    func getMessages() -> [Message] {
        return self.messages
    }
    
    private func parseJSON(json: JSON) {
        if let messages = json["chats"].array {
            for msg in messages {
                let message = Message()
                message!.username = msg["username"].string
                message!.content = msg["content"].string
                message!.time = msg["time"].string
                message!.imageURL = msg["userImage_url"].string
                
                self.messages.append(message!)
            }
        }
    }
}
