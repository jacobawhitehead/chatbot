//
//  ViewController.swift
//  ChatBot
//
//  Created by Muge Ersoy on 21/04/2016.
//  Copyright © 2016 Schibsted. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView.scrollEnabled = false

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.viewTapped))
        self.view.addGestureRecognizer(tap)
    }

    @IBAction func showChat(sender: AnyObject) {
        if login() {
            self.performSegueWithIdentifier("gotoChat", sender: nil)
        } else {
            let alert = UIAlertController(title: "Not valid", message: "Please enter your full name to login", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
        
    }
    
    func login() -> Bool {
        let username = self.loginTextField.text!
        
        if loginInputIsValid() {
            SessionManager.currentSession.loginWithUsername(username)
            return true
        } else {
            return false
        }
    }
    
    @objc private func viewTapped() {
        self.view.endEditing(true)
    }
    
    private func loginInputIsValid() -> Bool {
        let fullName = self.loginTextField.text

        let fullNameArr = fullName!.characters.split{$0 == " "}.map(String.init)
        
        if fullNameArr.count > 1 {
            return true
        }
        
        return false
    }
    
    //MARK - Keyboard
    func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
    
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboardFrame.height, 0)
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
        
        let offset = -(self.loginTextField.frame.origin.y - keyboardFrame.height)
        scrollView.setContentOffset(CGPointMake(0, offset), animated: true)
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsetsZero;
        scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }
    
}

