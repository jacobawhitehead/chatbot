//
//  ChatBubble.swift
//  ChatBot
//
//  Created by Muge Ersoy on 22/04/2016.
//  Copyright © 2016 Schibsted. All rights reserved.
//


import UIKit


class ChatBubble : UITableViewCell {

    @IBOutlet weak var bubbleContent: UILabel!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var background: UIView!
    
    @IBOutlet weak var bubbleView: UIView!
    
    @IBOutlet weak var userImage: UIProfileViewImage!
    
    override func awakeFromNib() {
        self.userImage.layer.masksToBounds = true
        self.userImage.layer.borderWidth = 1
        self.userImage.layer.borderColor = UIColor.whiteColor().CGColor
        self.userImage.layer.cornerRadius = 15

        self.bubbleView.layer.cornerRadius = 8
        self.bubbleView.layer.masksToBounds = true
    }
    
    func updateChatBubble(message : Message) {
        let usernameText = message.username + " - " + message.time
        self.userName.text = usernameText
        self.bubbleContent.text = message.content
        self.userImage.loadProfilePictureFromURL(message.imageURL, username: message.username)
    }
}