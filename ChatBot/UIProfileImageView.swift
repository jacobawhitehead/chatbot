//
//  UIProfileImageView.swift
//  ChatBot
//
//  Created by Jacob Whitehead on 06/06/2016.
//  Copyright © 2016 Schibsted. All rights reserved.
//

import UIKit

class UIProfileViewImage: UIImageView {
    private let defaultProfileImage = UIImage(named: "social")
    private var username = ""
    
    func loadProfilePictureFromURL(url: String, username: String) {
        self.username = username
        
        self.loadImageFromCache({ (succeeded) in
            if succeeded {
                //do nothing
            } else {
                self.loadImageFromURL(url)
            }
        })
    }

    private func loadImageFromURL(urlString: String) {
        if let checkedUrl = NSURL(string: urlString) {
            downloadImage(checkedUrl)
        }
    }
    
    private func styleImageView() {
        self.layer.cornerRadius = self.frame.size.width/2
        self.layer.masksToBounds = true
    }
    
    private func getDataFromUrl(url:NSURL, completion: ((data: NSData?, response: NSURLResponse?, error: NSError? ) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            completion(data: data, response: response, error: error)
            }.resume()
    }
    
    private func downloadImage(url: NSURL){
        getDataFromUrl(url) { (data, response, error)  in
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                guard let data = data where error == nil else {
                    self.image = self.defaultProfileImage
                    return
                }
                let image = UIImage(data: data)
                if (image != nil) {
                    self.image = image
                    self.cacheImage(image!)
                } else {
                    self.image = self.defaultProfileImage
                }
            }
        }
    }
    
    //caching
    private func cacheImage(image:UIImage) {
        if let data = UIImagePNGRepresentation(image) {
            data.writeToFile(self.imagePath(), atomically: true)
        }
    }
    
    private func loadImageFromCache(completion: (succeeded: Bool)->()) {
        let image = UIImage(contentsOfFile: self.imagePath())
        if (image != nil) {
            self.image = image
            completion(succeeded: true)
        } else {
            completion(succeeded: false)
        }
    }
    
    private func imagePath() -> String {
        return getDocumentsDirectory().stringByAppendingPathComponent(self.username + ".png")
    }
    
    private func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

}