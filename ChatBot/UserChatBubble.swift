//
//  UserChatBubble.swift
//  ChatBot
//
//  Created by Jacob Whitehead on 08/06/2016.
//  Copyright © 2016 Schibsted. All rights reserved.
//

import UIKit


class UserChatBubble : UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var bubbleView: UIView!
    
    override func awakeFromNib() {
        self.bubbleView.layer.cornerRadius = 8
        self.bubbleView.layer.masksToBounds = true
    }
    
    func updateChatBubble(message : Message) {
        self.dateLabel.text = message.time
        self.contentLabel.text = message.content
    }
}
