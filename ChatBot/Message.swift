//
//  Message.swift
//  ChatBot
//
//  Created by Muge Ersoy on 22/04/2016.
//  Copyright © 2016 Schibsted. All rights reserved.
//

import Foundation

class Message {

    var username : String!
    
    var content: String!
    
    var imageURL: String!

    var time : String!

    init?(){
    }
    
    init(content: String) {
        self.username = SessionManager.currentSession.currentUser()
        self.content = content
        self.setCurrentDate()
        self.imageURL = ""
    }
    
    private func setCurrentDate() {
        let currentDate = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm"
        
        self.time = formatter.stringFromDate(currentDate)
    }
}