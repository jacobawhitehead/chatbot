//
//  ChatBotTests.swift
//  ChatBotTests
//
//  Created by Muge Ersoy on 21/04/2016.
//  Copyright © 2016 Schibsted. All rights reserved.
//

import XCTest
@testable import ChatBot

class ChatBotTests: XCTestCase {
    var vc:LoginViewController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        vc = storyboard.instantiateViewControllerWithIdentifier("loginViewController") as! LoginViewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLoginSuccess() {
        let _ = vc.view
        vc.loginTextField.text = "Test User"
        
        XCTAssertTrue(vc.login())
    }
    
    func testLoginFail() {
        let _ = vc.view
        vc.loginTextField.text = "Test"
        
        XCTAssertFalse(vc.login())
    }
    
    func testPostSuccess() {
        
    }
    
}
